param (
    [string]$ProjectFile
)

[xml]$Project = Get-Content $ProjectFile

$ReleaseConfig = $Project.Project.ItemGroup.ClCompile `
                 | Where-Object { $_.Include -eq "Release" }

if ($ReleaseConfig) {
    $ReleaseConfig.RuntimeLibrary = "MultiThreaded"
    $Project.Save($ProjectFile)
    Write-Host "Updated runtime library to MT for Release configuration."
} else {
    Write-Host "Release configuration not found. No changes made."
}

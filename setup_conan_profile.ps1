$defaultProfilePath = "c:/cache/.conan/profiles/default"

if (-not (Test-Path $defaultProfilePath)) {
    New-Item -ItemType Directory -Path "c:/cache/.conan/profiles" -Force
    @"
[settings]
arch=x86_64
arch_build=x86_64
build_type=Release
os_build=Windows
os=Windows
compiler=msvc
compiler.version=192
compiler.runtime=static
compiler.cppstd=17
"@ | Set-Content -Path $defaultProfilePath
}

# escape=`

# Use the latest Windows Server Core 2019 image.
ARG FROM_IMAGE=mcr.microsoft.com/windows/servercore:ltsc2019
FROM ${FROM_IMAGE}

# Restore the default Windows shell for correct batch processing.
SHELL ["cmd", "/S", "/C"]

# Copy our Install scriptRUN echo "Copying Install.cmd to C:\\TEMP\\".
COPY Install.cmd C:/TEMP/

# Download collect.exe in case of an install failure.
ADD https://aka.ms/vscollect.exe C:/TEMP/collect.exe
# Install Node.js LTS
ADD https://nodejs.org/dist/v8.11.3/node-v8.11.3-x64.msi C:/TEMP/node-install.msi
RUN start /wait msiexec.exe /i C:\TEMP\node-install.msi /l*vx "%TEMP%\MSI-node-install.log" /qn ADDLOCAL=ALL
# Use the latest release channel. For more control, specify the location of an internal layout.
ARG CHANNEL_URL=https://aka.ms/vs/16/release/channel
ADD ${CHANNEL_URL} C:\TEMP\VisualStudio.chman

ADD https://aka.ms/vs/16/release/vs_buildtools.exe C:/TEMP/vs_buildtools.exe
RUN C:\TEMP\Install.cmd C:\TEMP\vs_buildtools.exe --quiet --wait --norestart --nocache `
    --channelUri C:\TEMP\VisualStudio.chman `
    --installChannelUri C:\TEMP\VisualStudio.chman `
    --add Microsoft.VisualStudio.Workload.VCTools --includeRecommended`
    --installPath C:\BuildTools



# ========== setup chocolatey package manager and tools ==========
RUN powershell.exe -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SETX PATH "%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

RUN choco install -y git.install
RUN choco install -y python39

RUN py -m pip install cmake
RUN py -m pip install conan==1.59

# we'll mount a volume from the host where the conan packages will be installed
ENV CONAN_USER_HOME="c:/cache"
ENV CONAN_USER_HOME_SHORT="c:/cache/conan_shortpaths"
ENV CONAN_DEFAULT_PROFILE_PATH "c:/cache/.conan/profiles/default"

COPY setup_conan_profile.ps1 C:/TEMP/
COPY project_setup.ps1 C:/TEMP/

# create the docker volume
VOLUME ["c:/cache"]

ENTRYPOINT ["C:\\BuildTools\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]
CMD ["-Command", "C:/TEMP/setup_conan_profile.ps1; C:/TEMP/project_setup.ps1; Start-Sleep -Seconds 86400"]
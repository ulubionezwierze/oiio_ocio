$workspacePath = "C:/workspace"
Remove-Item -Path $workspacePath -Recurse -Force
New-Item -ItemType Directory -Path $workspacePath
Set-Location $workspacePath


#git config --global http.sslVerify false
git config --global http.sslBackend schannel
git config --global http.schannelCheckRevoke false
git clone https://gitlab.pi/pipeline-test/oiio_build/oiio_build

Set-Location oiio_ocio/MD5Encrypter

New-Item -ItemType Directory -Path build
Set-Location build

conan install .. --build missing -pr:b=default

cmake .. -G "Visual Studio 16 2019" -A x64

& "..\update_runtime_to_MT.ps1" ".\MD5Encrypter.vcxproj"

& "..\cmake_build_and_run.ps1"
